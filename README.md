Matix Plugins Builder
=====================

- Execution Batch File:
    - `java -jar MatixPluginsBuilder.jar %*`
- Build Plugins:
    - `matixplugins.bat build`
- Clean Maven Projects:
    - `matixplugins.bat clean`
- Create Plugins:
    - `matixplugins.bat create <plugin name> <matix version (maven artifact version)>`
    - Will be using `1.10-1.6.3.1B` as default version