package de.paxii.matix.pluginsbuilder.file

import java.io.File
import java.io.FileWriter
import java.io.InputStream
import java.util.*

/**
 * Created by Lars on 19.06.2016.
 */

fun getFileContents(inputStream: InputStream): String {
    var fileContents = String()
    val scanner = Scanner(inputStream)

    while (scanner.hasNextLine()) {
        fileContents += scanner.nextLine() + System.getProperty("line.separator")
    }
    scanner.close()

    return fileContents
}

fun setFileContents(filePath: File, fileContents: String) {
    if (!filePath.exists()) {
        filePath.parentFile.mkdirs()
        filePath.createNewFile()
    }

    val fileWriter = FileWriter(filePath)
    fileWriter.write(fileContents)
    fileWriter.close()
}