package de.paxii.matix.pluginsbuilder

/**
 * Created by Lars on 05.06.2016.
 */
enum class Goal {
    UNDEFINED,
    CREATE,
    BUILD,
    DEPLOY,
    CLEAN
}