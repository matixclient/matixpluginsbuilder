package de.paxii.matix.pluginsbuilder

import de.paxii.matix.pluginsbuilder.file.getFileContents
import de.paxii.matix.pluginsbuilder.file.setFileContents
import java.io.File
import java.io.FileFilter
import java.util.*
import javax.xml.parsers.DocumentBuilderFactory

/**
 * Created by Lars on 05.06.2016.
 */

class PluginsBuilder {
    var matixVersion = "1.10-1.6.3.1B"

    fun main(args: Array<String>) {
        var goal = Goal.UNDEFINED

        if (args.size > 0) {
            if (args[0].equals("create", true)) {
                goal = Goal.CREATE
            } else if (args[0].equals("build", true)) {
                goal = Goal.BUILD
            } else if (args[0].equals("deploy", true)) {
                goal = Goal.DEPLOY
            } else if (args[0].equals("clean", true)) {
                goal = Goal.CLEAN
            } else {
                println("Available goals: create, build, deploy")
            }
        } else {
            println("Available goals: build, deploy")
        }

        if (goal == Goal.UNDEFINED) {
            System.exit(1)
        }

        val workingDirectory = File(".")

        if (goal == Goal.CREATE) {
            if (args.size <= 1) {
                println("Please supply an artifact id!")
                System.exit(1)
            }

            val artifactId = args[1]
            if (args.size >= 3) {
                matixVersion = args[2]
            }

            val targetDir = File(workingDirectory, "$artifactId/")
            val resourcesDir = File(workingDirectory, "$artifactId/src/main/resources/")
            val servicesDir = File(workingDirectory, "$artifactId/src/main/resources/META-INF/services/")

            val pomContents = getFileContents(this.javaClass.getResourceAsStream("/templates/pom.xml"))
            val gitignoreContents = getFileContents(this.javaClass.getResourceAsStream("/templates/.gitignore"))
            val moduleJsonContents = getFileContents(this.javaClass.getResourceAsStream("/templates/module.json"))
            val servicesContents = getFileContents(this.javaClass.getResourceAsStream("/templates/META-INF/services/de.paxii.clarinet.module.Module"))

            File(targetDir, "src/main/java").mkdirs()
            File(targetDir, "src/main/resources/META-INF/services").mkdirs()
            setFileContents(File(targetDir, "pom.xml"), pomContents.format(artifactId, matixVersion))
            setFileContents(File(targetDir, ".gitignore"), gitignoreContents)
            setFileContents(File(resourcesDir, "module.json"), moduleJsonContents.format(artifactId))
            setFileContents(File(servicesDir, "de.paxii.clarinet.module.Module"), servicesContents.format(artifactId))

            return
        }

        val buildDirectory = File(workingDirectory, ".BUILD/")
        val pluginDirectories = workingDirectory.listFiles(FileFilter { it.isDirectory && !it.name.startsWith(".") })

        buildDirectory.mkdir()

        pluginDirectories.forEach {
            val pom = File(it, "pom.xml")
            val targetDirectory = File(it, "target/")

            if (pom.exists()) {
                val documentBuilderFactory = DocumentBuilderFactory.newInstance()
                val documentBuilder = documentBuilderFactory.newDocumentBuilder()
                val document = documentBuilder.parse(pom)

                document.documentElement.normalize()
                val artifactId = document.getElementsByTagName("artifactId").item(0).textContent
                val version = document.getElementsByTagName("version").item(0).textContent

                val targetFile = File(targetDirectory, "$artifactId-$version-jar-with-dependencies.jar")

                when (goal) {
                    Goal.BUILD -> {
                        if (!targetFile.exists()) {
                            println("Building Artifact $artifactId with Version $version")

                            val mavenProcess = Runtime.getRuntime().exec("cmd /c mvn -f ./$artifactId/pom.xml package")
                            mavenProcess.waitFor()
                        }

                        if (targetFile.exists()) {
                            val pluginBuildDirectory = File(buildDirectory, "$artifactId")
                            pluginBuildDirectory.mkdir()

                            println("Copying Artifact $artifactId with Version $version")
                            targetFile.copyTo(File(pluginBuildDirectory, "$artifactId-$version.jar"), true)
                        } else {
                            println("There was an Error with Artifact $artifactId")
                        }
                    }
                    Goal.DEPLOY -> {
                        println("Deploying Artifact $artifactId with Version $version")
                        val mavenProcess = Runtime.getRuntime().exec("cmd /c mvn -f ./$artifactId/pom.xml deploy")
                        val scanner = Scanner(mavenProcess.inputStream)

                        while (scanner.hasNextLine()) {
                            println(scanner.nextLine())
                        }
                    }
                    Goal.CLEAN -> {
                        println("Cleaning Artifact $artifactId")
                        val mavenProcess = Runtime.getRuntime().exec("cmd /c mvn -f ./$artifactId/pom.xml clean")
                        mavenProcess.waitFor()
                    }
                    else -> System.exit(1)
                }
            }
        }
    }
}